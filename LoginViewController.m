//
//  LoginViewController.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import "LoginViewController.h"
#import "PushAndPopUtility.h"
#import "CoreDataUtility.h"
#import "OLGhostAlertView.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [CoreDataUtility fetchDatafromTable:@"Person" predicate:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*!
 @brief Present the second view controller on first one.
 @discussion We set the deflaut user Name and password for login It moves to next view Controller .
 */
- (IBAction)loginPressed:(id)sender {

    
    if ([[self.UserNameTextField.text lowercaseString]isEqualToString:@"vivek@gmail.com"] && [self.passWordTextField.text isEqualToString:@"123456789"]) {
        [PushAndPopUtility pushPresentViewControllerWithStoryBoardName:@"Main" viewControllerName:@"trasnscation" viewContoller:self];

    }else{
        [PushAndPopUtility olghostWithVc:self title:@"InValid Credinatial" message:@"Enter Correct Email Id And Password"];
    }
}
- (IBAction)signInPressed:(id)sender {
     [PushAndPopUtility olghostWithVc:self title:@"" message:@"Under Constraction"];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
