//
//  LoginViewController.h
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *UserNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passWordTextField;

@end
