//
//  ViewController.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 05/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import "AssetListViewController.h"
#import "PushAndPopUtility.h"
#import "MySharedData.h"
#import "igViewController.h"


@interface AssetListViewController ()

@property (weak, nonatomic) IBOutlet UITextField *serialNumberText;

@property (weak, nonatomic) IBOutlet UITextField *modelNumber;

@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation AssetListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    _locationManager =[[CLLocationManager alloc]init];
    // Use either one of these authorizations **The top one gets called first and the other gets ignored
    [self.locationManager requestWhenInUseAuthorization];
    //[self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    //    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
    //                                                  target:self
    //                                                selector:@selector(checkStatus)
    //                                                userInfo:nil repeats:YES];
    
    //    [self checkStatus];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(checkStatus)
                                                    userInfo:nil repeats:YES];
        
    NSLog(@"%@", [self deviceLocation]);

    CLLocation *location =  self.locationManager.location;
    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks lastObject];
        
        NSString *street = placemark.thoroughfare;
        NSString *sublocality =placemark.subLocality;
        NSString *city = placemark.locality;
        NSString *posCode = placemark.postalCode;
        NSString *country = placemark.country;
        
        self.locationTextField.text = [NSString stringWithFormat:@"%@ \n%@ \n%@",street,sublocality,city];
        NSLog(@"we live in %@ ,%@, %@, %@, %@",street,sublocality,city,country,posCode);
    }];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 400);
    
    // Do any additional setup after loading the view, typically from a nib.
}


-(void)viewDidAppear:(BOOL)animated{
    MySharedData *sharedata =[MySharedData sharedManager];
    NSArray *tagArray =[ sharedata.barCodeId componentsSeparatedByString:@"#"];
    if (![sharedata.barCodeId isEqualToString:@""] && tagArray.count > 1) {
        if ([[tagArray objectAtIndex:1] isEqualToString:@"1"]) {
            self.serialNumberText.text = [tagArray objectAtIndex:0];
            sharedata.barCodeId = @"";
        }else if ([[tagArray objectAtIndex:1] isEqualToString:@"2"]){
            self.modelNumber.text =  [tagArray objectAtIndex:0];
            sharedata.barCodeId = @"";
        }
    }
}
- (IBAction)backPressed:(id)sender {
    
    [[self navigationController]popViewControllerAnimated:YES];
    
    
}
- (IBAction)savePressed:(id)sender {
    
    [PushAndPopUtility olghostWithVc:self title:@"Saving Data" message:@"Saved"];
    [self performSelector:@selector(backPressed:) withObject:self afterDelay:0.7];
}

- (IBAction)barCodebtnPressed:(UIButton*)sender {
    
    UIStoryboard *mainStoryboard;
   igViewController  *vc;
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ig"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.tag = sender.tag;
    [[self navigationController]pushViewController:vc animated:YES];

}

-(void)checkStatus{
    
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status==kCLAuthorizationStatusNotDetermined) {
        NSLog(@"");
        //        _status.text = @"Not Determined";
    }
    if (status==kCLAuthorizationStatusDenied) {//
        
        
        NSLog(@"");
        
        //        _status.text = @"Denied";
    }
    if (status==kCLAuthorizationStatusRestricted) {
        NSLog(@"");
        
        //        _status.text = @"Restricted";
    }
    if (status==kCLAuthorizationStatusAuthorizedAlways) {
        NSLog(@"");
        
        //        _status.text = @"Always Allowed";
    }
    if (status==kCLAuthorizationStatusAuthorizedWhenInUse) {
        NSLog(@"");
        
        //        _status.text = @"When In Use Allowed";
        
    }
}


/*!
 @brief it Finds the location of user.
 
 */
- (IBAction)LocationFinderPressed:(id)sender {
    
    
    
    
    
    
    
    
    
}

- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", _locationManager.location.coordinate.latitude, _locationManager.location.coordinate.longitude];
}


-(void)setLabel:(float)xPosition yPosition:(float)yPosition width:(float)width height:(float)height text:(NSString *)text{
    UILabel *label =[[UILabel alloc]init];
    label.frame = CGRectMake(xPosition, yPosition, width, height);
    label.text =text;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
