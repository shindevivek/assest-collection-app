//
//  main.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 05/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
