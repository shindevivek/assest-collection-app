//
//  CustomAsset.h
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomAsset : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CustomAsset+CoreDataProperties.h"
