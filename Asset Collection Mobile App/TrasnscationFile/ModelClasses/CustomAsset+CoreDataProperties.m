//
//  CustomAsset+CoreDataProperties.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CustomAsset+CoreDataProperties.h"

@implementation CustomAsset (CoreDataProperties)

@dynamic refernceID;
@dynamic name;
@dynamic value;

@end
