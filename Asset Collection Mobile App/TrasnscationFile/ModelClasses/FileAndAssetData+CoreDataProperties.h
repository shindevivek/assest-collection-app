//
//  FileAndAssetData+CoreDataProperties.h
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 07/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FileAndAssetData.h"

NS_ASSUME_NONNULL_BEGIN

@interface FileAndAssetData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *type;

@end

NS_ASSUME_NONNULL_END
