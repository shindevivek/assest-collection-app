//
//  CustomAsset+CoreDataProperties.h
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CustomAsset.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomAsset (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *refernceID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *value;

@end

NS_ASSUME_NONNULL_END
