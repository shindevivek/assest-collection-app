//
//  FileAndAssetData+CoreDataProperties.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 07/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FileAndAssetData+CoreDataProperties.h"

@implementation FileAndAssetData (CoreDataProperties)

@dynamic name;
@dynamic type;

@end
