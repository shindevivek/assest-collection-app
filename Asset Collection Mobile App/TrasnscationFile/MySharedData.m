//
//  MySharedData.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import "MySharedData.h"

@implementation MySharedData

static MySharedData *sharedData = nil;

+ (id)sharedManager {
    @synchronized(self) {
        if (sharedData == nil)
            sharedData = [[self alloc] init];
    }
    return sharedData;
}
-(instancetype)init{
    if (self =[super init]) {
        
    }
    return self;
}

@end
