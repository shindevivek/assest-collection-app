//
//  PushAndPopUtility.h
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushAndPopUtility : UIViewController
+(void)pushPresentViewControllerWithStoryBoardName:(NSString *)storyBoardName viewControllerName:(NSString *)viewControllerName viewContoller:(UIViewController *)viewContoller;
+(void)olghostWithVc:(UIViewController *)vc title:(NSString *)title message:(NSString *)message;
+(void)dismissController:(UIViewController *)viewController;
@end
