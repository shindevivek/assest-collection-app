//
//  MySharedData.h
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySharedData : NSObject
+ (id)sharedManager;


@property (nonatomic, retain) NSString *barCodeId;




@end
