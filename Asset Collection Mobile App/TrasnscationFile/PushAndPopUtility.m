//
//  PushAndPopUtility.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import "PushAndPopUtility.h"
#import "OLGhostAlertView.h"

@implementation PushAndPopUtility

+(void)pushPresentViewControllerWithStoryBoardName:(NSString *)storyBoardName viewControllerName:(NSString *)viewControllerName viewContoller:(UIViewController *)viewContoller{
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:viewControllerName];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [[viewContoller navigationController]pushViewController:vc animated:YES];
}

+(void)olghostWithVc:(UIViewController *)vc title:(NSString *)title message:(NSString *)message{
    OLGhostAlertView *alertView = [[OLGhostAlertView alloc] initWithTitle:title message:@"" timeout:3.0 dismissible:YES];
    alertView.position = OLGhostAlertViewPositionCenter;
    alertView.style = OLGhostAlertViewStyleDark;
    alertView.message = message;
    [alertView showInView:vc.view];
}


+(void)dismissController:(UIViewController *)viewController{
    [viewController popoverPresentationController];
}


@end
