//
//  TransactionFileViewController.m
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 06/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import "TransactionFileViewController.h"
#import "PushAndPopUtility.h"
#import "MenuTableViewCell.h"
#import "CoreDataUtility.h"
#import "FileAndAssetData.h"


@interface TransactionFileViewController ()
@property (strong, nonatomic) IBOutlet UITableView *menuTable;
@property (strong, nonatomic) IBOutlet UIImageView *bar;
@end

@implementation TransactionFileViewController{
    NSArray *menuArray;

}

- (void)viewDidLoad {
    [super viewDidLoad];

    menuArray = [CoreDataUtility fetchDatafromTable:@"FileAndAssetData" predicate:[NSPredicate predicateWithFormat:@"type == %@",@"Transcation"]];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 @brief Transaction View
 
 
 
 */
-(void)setBarFrame:(UIButton *)button{
    [UIView animateWithDuration:0.3 animations:^{
        self.bar.frame = CGRectMake(button.frame.origin.x, self.bar.frame.origin.y, button.frame.size.width, self.bar.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}



- (IBAction)buttonPressed:(UIButton *)sender {
    [self setBarFrame:sender];
    
    if ([sender.titleLabel.text isEqualToString:@"Transcation"] ) {

        menuArray = [CoreDataUtility fetchDatafromTable:@"FileAndAssetData" predicate:[NSPredicate predicateWithFormat:@"type == %@",@"Transcation"]];
    }else{
        menuArray = [CoreDataUtility fetchDatafromTable:@"FileAndAssetData" predicate:[NSPredicate predicateWithFormat:@"type == %@",@"Files"]];
   
    }
    [self.menuTable reloadData];
  }


#pragma mark -UITableView Source code


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //warning Incomplete method implementation.
    // Return the number of rows in the section.
    //return array1.count;
    
    return menuArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"topicCell";
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath ];
    
    if ([cell.topicName.text isEqualToString:@"Asset Receive"] ) {
        [PushAndPopUtility pushPresentViewControllerWithStoryBoardName:@"Main" viewControllerName:@"AssetList" viewContoller:self];
    }else{
        [PushAndPopUtility olghostWithVc:self title:@"" message:@"Under Constraction"];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"topicCell";
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath ];
    if (cell == nil) {
        cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor =[UIColor clearColor];
    if (indexPath.row<menuArray.count) {
        FileAndAssetData *File =[menuArray objectAtIndex:indexPath.row];
        cell.topicName.text =  File.name;
        cell.delegate = self;
        cell.detailButton.tag=indexPath.row;
    }
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}



#pragma mark -UITableView End code




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
