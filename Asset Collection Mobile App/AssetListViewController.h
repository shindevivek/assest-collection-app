//
//  ViewController.h
//  Asset Collection Mobile App
//
//  Created by Vivek LogTera on 05/03/16.
//  Copyright © 2016 Logtera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>


@interface AssetListViewController : UIViewController<CLLocationManagerDelegate>
@property(nonatomic)CLLocationManager *locationManager;
@property(nonatomic)NSTimer *timer;

@end

