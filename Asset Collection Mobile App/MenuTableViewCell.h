//
//  WeeklyTableViewCell.h
//  Grade3Full
//
//  Created by Vivek LogTera on 07/01/15.
//  Copyright (c) 2015 LogTera3. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol TableCellDelegate
@optional
- (void)showDetailsofTopicInWeekly:(UIButton *)sender;

@end


@interface MenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *topicName;
@property (weak, nonatomic) IBOutlet UILabel *lastWeekQuestionAttemptedLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastWeekScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *overAllQuestionAttemptedLabel;
@property (weak, nonatomic) IBOutlet UILabel *overAllScore;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (nonatomic, strong) id  delegate;

@end