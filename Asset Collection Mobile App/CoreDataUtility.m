//
//  CoreDataUtility.m
//  Grade3 App
//
//  Created by Logtera on 30/07/14.
//  Copyright (c) 2014 LogTera3. All rights reserved.
//

#import "CoreDataUtility.h"
#import "AppDelegate.h"


@interface CoreDataUtility()

//@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end


@implementation CoreDataUtility


+(NSArray *)fetchDatafromTable :(NSString *)tableName predicate:(NSPredicate *)predicate{
    NSArray *array = nil;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    if (context){
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *fetchentity = [NSEntityDescription entityForName:tableName
                                                       inManagedObjectContext:context];
        [fetchRequest setEntity:fetchentity];
        
        if (predicate !=Nil) {
            [fetchRequest setPredicate:predicate];
        }
        
        NSError *error = nil;
        array = [context executeFetchRequest:fetchRequest error:&error];
        return array;
    } else {
        return array;
        
    }
}

// retrun type YES  if recored is deleted
//deleteDatafromTable :-parameter  1)tableName 2)predicate;
+(BOOL)deleteDatafromTable :(NSString *)tableName predicate:(NSPredicate *)predicate{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *fetchentity = [NSEntityDescription entityForName:tableName
                                                   inManagedObjectContext:context];
    [fetchRequest setEntity:fetchentity];
    
    if (predicate !=Nil) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:fetchRequest error:&error];
    
    
    for (id managedObject in array) {
        [context deleteObject:managedObject];
    }
    
    
    NSError *error1 = nil;
    if (![context save:&error1]) {
        NSLog(@"Can't Delete! %@ %@", error1, [error1 localizedDescription]);
        return NO;//record is not deleted
    }
    
    return YES;////record is not deleted
}


+(NSArray *)fetchDatafromTableWithDistinct :(NSString *)tableName predicate:(NSPredicate *)predicate isDistinct:(BOOL)isDistinct DistinctKey:(NSString *)distinctKey{
    NSArray *array = nil;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    if (context){
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *fetchentity = [NSEntityDescription entityForName:tableName
                                                       inManagedObjectContext:context];
        [fetchRequest setEntity:fetchentity];
        if(isDistinct)
        [fetchRequest setResultType:NSDictionaryResultType];
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:[[fetchentity propertiesByName] objectForKey:distinctKey]]]; //@"sub_topic_id"
        [fetchRequest setReturnsDistinctResults:YES];
        if (predicate !=Nil) {
            [fetchRequest setPredicate:predicate];
        }
        NSError *error = nil;
        array = [context executeFetchRequest:fetchRequest error:&error];
        return array;
    } else {
        return array;
    }
}


+(NSInteger )fetchNumberOfRecordfromTable :(NSString *)tableName predicate:(NSPredicate *)predicate{
    NSInteger numberCount = -1;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    if (context){
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *fetchentity = [NSEntityDescription entityForName:tableName
                                                       inManagedObjectContext:context];
        [fetchRequest setEntity:fetchentity];
        
        if (predicate !=Nil) {
            [fetchRequest setPredicate:predicate];
        }
        
        NSError *error = nil;
//        numberCount = [context executeFetchRequest:fetchRequest error:&error];
        numberCount = [context countForFetchRequest:fetchRequest error:&error];
        return numberCount;
    } else {
        return numberCount;
    }
}


@end
