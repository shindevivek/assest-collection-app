//
//  CoreDataUtility.h
//  Grade3 App
//
//  Created by Logtera on 30/07/14.
//  Copyright (c) 2014 LogTera3. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataUtility : NSObject

+(NSArray *)fetchDatafromTable :(NSString *)tableName predicate:(NSPredicate *)predicate;
+(BOOL)deleteDatafromTable :(NSString *)tableName predicate:(NSPredicate *)predicate;
+(NSArray *)fetchDatafromTableWithDistinct :(NSString *)tableName predicate:(NSPredicate *)predicate isDistinct:(BOOL)isDistinct DistinctKey:(NSString *)distinctKey;
+(NSInteger )fetchNumberOfRecordfromTable :(NSString *)tableName predicate:(NSPredicate *)predicate;
@end
