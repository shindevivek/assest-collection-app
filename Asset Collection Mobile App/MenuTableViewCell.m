//
//  WeeklyTableViewCell.m
//  Grade3Full
//
//  Created by Vivek LogTera on 07/01/15.
//  Copyright (c) 2015 LogTera3. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)showDetailsofTopicInWeekly:(UIButton *)sender {
    
    [self.delegate showDetailsofTopicInWeekly:sender];
    
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
